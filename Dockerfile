FROM golang:1.23.1-alpine AS build

RUN apk add --update --no-cache git

COPY ./api /go/src/scheme-service/api
COPY ./cmd /go/src/scheme-service/cmd
COPY ./internal /go/src/scheme-service/internal
COPY ./pkg /go/src/scheme-service/pkg
COPY ./go.mod /go/src/scheme-service/
COPY ./go.sum /go/src/scheme-service/
WORKDIR /go/src/scheme-service
RUN go mod download \
 && go build -o dist/bin/scheme-service ./cmd/scheme-service

# Release binary on latest alpine image.
FROM alpine:3.20

ARG USER_ID=10001
ARG GROUP_ID=10001

COPY --from=build /go/src/scheme-service/dist/bin/scheme-service /usr/local/bin/scheme-service
COPY --from=build /go/src/scheme-service/api/openapi.json /api/openapi.json
COPY --from=build /go/src/scheme-service/api/openapi.yaml /api/openapi.yaml

# Add non-priveleged user. Disabled for openshift
RUN addgroup -g "$GROUP_ID" appgroup \
 && adduser -D -u "$USER_ID" -G appgroup appuser
USER appuser
HEALTHCHECK none
CMD ["/usr/local/bin/scheme-service"]
