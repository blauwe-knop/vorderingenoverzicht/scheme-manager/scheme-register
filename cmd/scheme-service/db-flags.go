// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package main

import "fmt"

type SchemeDB struct {
	Host     string `long:"scheme-db-host" env:"SCHEME_DB_HOST" default:"scheme-db-rw" description:"SchemeDB host"`
	Port     uint16 `long:"scheme-db-port" env:"SCHEME_DB_PORT" default:"5432" description:"SchemeDB port"`
	Username string `long:"scheme-db-username" env:"SCHEME_DB_USERNAME" description:"SchemeDB username"`
	Password string `long:"scheme-db-password" env:"SCHEME_DB_PASSWORD" description:"SchemeDB password"`
	Database string `long:"scheme-db-database" env:"SCHEME_DB_DATABASE" default:"scheme-db" description:"SchemeDB database name"`
	SSLMode  string `long:"scheme-db-sslmode" env:"SCHEME_DB_SSLMODE" default:"require" description:"SchemeDB sslmode"`

	MaxIdleConns_ int `long:"scheme-db-max-idle-conns" env:"SCHEME_DB_MAX_IDLE_CONNS" default:"5" description:"Maximum idle connections allowed in the pg db pool."`
	MaxOpenConns_ int `long:"scheme-db-max-open-conns" env:"SCHEME_DB_MAX_OPEN_CONNS" default:"10" description:"Maximum open connections allowed in the pg db pool."`
}

// DSN returns a DSN string for the SchemeDB parameters
func (d SchemeDB) DSN() string {
	return fmt.Sprintf("host=%s port=%d dbname=%s user=%s password=%s sslmode=%s", d.Host, d.Port, d.Database, d.Username, d.Password, d.SSLMode)
}

// DSNSafe returns a DSN string that is safe to print
func (d SchemeDB) DSNSafe() string {
	dCensored := d
	dCensored.Password = "CENSORED"
	return dCensored.DSN()
}

func (d SchemeDB) MaxIdleConns() int {
	return d.MaxIdleConns_
}

func (d SchemeDB) MaxOpenConns() int {
	return d.MaxOpenConns_
}
