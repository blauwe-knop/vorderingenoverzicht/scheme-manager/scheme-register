// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package main

import (
	"github.com/huandu/xstrings"
	_ "github.com/jackc/pgx/v4/stdlib" // imported to register pgx db/sql.Driver.
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-service/pkg/events"
)

type DBConnectionDetailer interface {
	DSN() string
	DSNSafe() string
	MaxIdleConns() int
	MaxOpenConns() int
}

func SetupDB(logger *zap.Logger, detailer DBConnectionDetailer) (*sqlx.DB, error) {
	event := events.SCMS_11
	logger.Log(event.GetLogLevel(), event.Message, zap.String("dsn", detailer.DSNSafe()), zap.Reflect("event", event))

	db, err := sqlx.Open("pgx", detailer.DSN())
	if err != nil {
		event := events.SCMS_13
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return nil, errors.Wrap(err, "failed opening database connection")
	}
	db.MapperFunc(xstrings.ToSnakeCase)
	db.SetMaxIdleConns(detailer.MaxIdleConns())
	db.SetMaxOpenConns(detailer.MaxOpenConns())
	err = db.Ping()
	if err != nil {
		event := events.SCMS_14
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return nil, errors.Wrap(err, "failed to ping database")
	}

	event = events.SCMS_12
	logger.Log(event.GetLogLevel(), event.Message, zap.String("dsn", detailer.DSNSafe()), zap.Reflect("event", event))

	return db, nil
}

func SetupDBOrFatal(logger *zap.Logger, detailer DBConnectionDetailer) *sqlx.DB {
	db, err := SetupDB(logger, detailer)
	if err != nil {
		event := events.SCMS_15
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.String("dsn", detailer.DSNSafe()), zap.Reflect("event", event))
	}
	return db
}
