// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package http_infra

import (
	"encoding/json"
	"errors"
	"net/http"

	"github.com/go-chi/chi/v5"
	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-service/internal/repositories"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-service/pkg/events"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-service/pkg/model"
)

func handlerList(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	logger := context.Value(loggerKey).(*zap.Logger)
	organizationRepository, _ := context.Value(organizationRepositoryKey).(repositories.OrganizationRepository)

	event := events.SCMS_18
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	organizations, err := organizationRepository.List(context)
	if err != nil {
		event = events.SCMS_28
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	responseWriter.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(responseWriter).Encode(*organizations)
	if err != nil {
		event = events.SCMS_16
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	event = events.SCMS_19
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}

func handlerCreate(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	logger := context.Value(loggerKey).(*zap.Logger)
	organizationRepository, _ := context.Value(organizationRepositoryKey).(repositories.OrganizationRepository)

	event := events.SCMS_20
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	var organization model.Organization
	err := json.NewDecoder(request.Body).Decode(&organization)
	if err != nil {
		event = events.SCMS_17
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	createdOrganization, err := organizationRepository.Create(context, organization)
	if err != nil {
		event = events.SCMS_29
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	responseWriter.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(responseWriter).Encode(*createdOrganization)
	if err != nil {
		event = events.SCMS_16
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	event = events.SCMS_21
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}

func handlerGet(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	logger := context.Value(loggerKey).(*zap.Logger)
	organizationRepository, _ := context.Value(organizationRepositoryKey).(repositories.OrganizationRepository)

	event := events.SCMS_22
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	organization, err := organizationRepository.Get(context, chi.URLParam(request, "oin"))
	if errors.Is(err, repositories.ErrOrganizationNotFound) {
		event = events.SCMS_30
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "Not Found", http.StatusNotFound)
		return
	}
	if err != nil {
		event = events.SCMS_31
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	responseWriter.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(responseWriter).Encode(*organization)
	if err != nil {
		event = events.SCMS_16
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	event = events.SCMS_23
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}

func handlerUpdate(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	logger := context.Value(loggerKey).(*zap.Logger)
	organizationRepository, _ := context.Value(organizationRepositoryKey).(repositories.OrganizationRepository)

	event := events.SCMS_24
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	var organization model.Organization
	err := json.NewDecoder(request.Body).Decode(&organization)
	if err != nil {
		event = events.SCMS_17
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	oin := chi.URLParam(request, "oin")

	_, err = organizationRepository.Get(context, oin)
	if err != nil {
		event = events.SCMS_31
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	err = organizationRepository.Update(context, organization, oin)
	if err != nil {
		event = events.SCMS_32
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	responseWriter.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(responseWriter).Encode(organization)
	if err != nil {
		event = events.SCMS_16
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	event = events.SCMS_25
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}

func handlerDelete(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	logger := context.Value(loggerKey).(*zap.Logger)
	organizationRepository, _ := context.Value(organizationRepositoryKey).(repositories.OrganizationRepository)

	event := events.SCMS_26
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	oin := chi.URLParam(request, "oin")

	organization, err := organizationRepository.Get(context, oin)
	if err != nil {
		event = events.SCMS_31
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	err = organizationRepository.Delete(context, oin)
	if err != nil {
		event = events.SCMS_33
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	responseWriter.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(responseWriter).Encode(*organization)
	if err != nil {
		event = events.SCMS_16
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	event = events.SCMS_27
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}
