// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repositories

import (
	"context"
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-service/pkg/model"
)

type OrganizationDBClient struct {
	logger *zap.Logger

	stmtCreate      *sqlx.Stmt
	stmtList        *sqlx.Stmt
	stmtRead        *sqlx.Stmt
	stmtUpdate      *sqlx.Stmt
	stmtDelete      *sqlx.Stmt
	stmtHealthCheck *sqlx.Stmt
}

var ErrOrganizationNotFound = errors.New("organization does not exist")

const (
	SQL_CREATE_ORGANIZATION = `
INSERT INTO scheme.organization (
	oin,
	name,
	discovery_url,
	public_key,
	approved,
	logo_url)
VALUES (
	$1,
	$2,
	$3,
	$4,
	$5,
	$6)`
	SQL_LIST_ORGANIZATIONS = `
SELECT
	*
FROM
	scheme.organization
`
	SQL_READ_ORGANIZATION = `
SELECT
	*
FROM
	scheme.organization
WHERE
	oin = $1
`
	SQL_UPDATE_ORGANIZATION = `
UPDATE
	scheme.organization
SET
	oin = $2,
	name = $3,
	discovery_url = $4,
	public_key = $5,
	approved = $6,
	logo_url = $7
WHERE
	oin = $1
`
	SQL_DELETE_ORGANIZATION = `
		DELETE FROM scheme.organization
		WHERE oin = $1
		`

	SQL_HEALTHCHECK_ORGANIZATION = `
		SELECT 1
		`
)

func NewOrganizationDBClient(ctx context.Context, logger *zap.Logger, schemeDB *sqlx.DB) (*OrganizationDBClient, error) {
	r := &OrganizationDBClient{logger: logger}
	var err error

	r.stmtList, err = schemeDB.PreparexContext(ctx, SQL_LIST_ORGANIZATIONS)

	if err != nil {
		return nil, errors.Wrap(err, "failed to create stmtReadOrganizations")
	}

	r.stmtCreate, err = schemeDB.PreparexContext(ctx, SQL_CREATE_ORGANIZATION)

	if err != nil {
		return nil, errors.Wrap(err, "failed to create stmtCreateOrganization")
	}

	r.stmtRead, err = schemeDB.PreparexContext(ctx, SQL_READ_ORGANIZATION)

	if err != nil {
		return nil, errors.Wrap(err, "failed to create stmtReadOrganizationByOin")
	}

	r.stmtUpdate, err = schemeDB.PreparexContext(ctx, SQL_UPDATE_ORGANIZATION)

	if err != nil {
		return nil, errors.Wrap(err, "failed to create stmtUpdateOrganization")
	}

	r.stmtDelete, err = schemeDB.PreparexContext(ctx, SQL_DELETE_ORGANIZATION)

	if err != nil {
		return nil, errors.Wrap(err, "failed to create stmtDeleteOrganization")
	}

	r.stmtHealthCheck, err = schemeDB.PreparexContext(ctx, SQL_HEALTHCHECK_ORGANIZATION)

	if err != nil {
		return nil, errors.Wrap(err, "failed to create stmtHealthCheck")
	}

	return r, nil
}

func (s *OrganizationDBClient) Create(ctx context.Context, organization model.Organization) (*model.Organization, error) {
	s.logger.Debug("Create", zap.String("organization.oin", organization.Oin))

	_, err := s.stmtCreate.ExecContext(ctx, organization.Oin, organization.Name, organization.DiscoveryUrl, organization.PublicKey, organization.Approved, organization.LogoUrl)

	if err != nil {
		return nil, errors.Wrap(err, "failed to create")
	}

	return &organization, nil
}

func (s *OrganizationDBClient) List(ctx context.Context) (*[]model.Organization, error) {
	s.logger.Debug("List")
	result := []model.Organization{}

	err := s.stmtList.SelectContext(ctx, &result)

	if err != nil {
		return nil, errors.Wrap(err, "failed to list")
	}

	return &result, nil
}
func (s *OrganizationDBClient) Get(ctx context.Context, oin string) (*model.Organization, error) {
	s.logger.Debug("Get", zap.String("oin", fmt.Sprint(oin)))
	result := []model.Organization{}

	err := s.stmtRead.SelectContext(ctx, &result, oin)

	if err != nil {
		return nil, errors.Wrap(err, "failed to get")
	}

	if len(result) == 0 {
		return nil, ErrOrganizationNotFound
	}

	return &result[0], nil
}

func (s *OrganizationDBClient) Update(ctx context.Context, organization model.Organization, oin string) error {
	s.logger.Debug("Update", zap.String("oin", fmt.Sprint(oin)))

	_, err := s.stmtUpdate.ExecContext(ctx, oin, organization.Oin, organization.Name, organization.DiscoveryUrl, organization.PublicKey, organization.Approved, organization.LogoUrl)

	if err != nil {
		return errors.Wrap(err, "failed to update")
	}

	return nil
}

func (s *OrganizationDBClient) Delete(ctx context.Context, oin string) error {
	s.logger.Debug("Delete", zap.String("oin", fmt.Sprint(oin)))
	_, err := s.stmtDelete.ExecContext(ctx, oin)

	if err != nil {
		return errors.Wrap(err, "failed to delete")
	}

	return nil
}

func (s *OrganizationDBClient) GetHealthCheck() healthcheck.Result {
	name := "scheme-db"
	start := time.Now()

	_, err := s.stmtHealthCheck.Exec()

	if err != nil {
		s.logger.Log(zap.ErrorLevel, "failed to check health: %v", zap.Error(err))
		return healthcheck.Result{
			Name:         name,
			Status:       healthcheck.StatusError,
			ResponseTime: time.Since(start).Seconds(),
			HealthChecks: []healthcheck.Result{},
		}
	}

	return healthcheck.Result{
		Name:         name,
		Status:       healthcheck.StatusOK,
		ResponseTime: time.Since(start).Seconds(),
		HealthChecks: []healthcheck.Result{},
	}

}
