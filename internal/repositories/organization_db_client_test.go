// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repositories_test

import (
	"context"
	"database/sql"
	"regexp"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"go.uber.org/zap/zaptest"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-service/internal/repositories"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-service/pkg/model"
)

var mockOrganization1 model.Organization = model.Organization{
	Oin:          "00000000000000000001",
	Name:         "mock-org 1",
	DiscoveryUrl: "http://mock1/v1",
	PublicKey: `-----BEGIN PUBLIC KEY-----
	MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxlibRfmDYIrD9MJRh0BS
	utHKiYlXeGopRHT2G8FK8WGvN2faR1ZFdWjTxgncmb1nds7OoG5u2E1KrDMiZ3SH
	M7T95Ynle1LHE9D0VhUXA0iAYOq4aumHiyHpmT8XQhRsF/3RjGoGagtnaeQ3uSyd
	NBkOiuVm/JTmcU3QDhLwj8st/GvxwaViuLsIH4WNX/LmkPkVbW5/CLNtD9mTmMgt
	x3tEBYHyIctPoBrXfYBaJN4tv6JY1vI37cafG4MBzunAr4iP/1GBhh6ql8bpdqd7
	cD6cHvGmlubrmdILm9V0erNDV9O9NrjxIalYRU/5E10qGIAuBdBubfOvJQX8QgFa
	WQIDAQAB
	-----END PUBLIC KEY-----`,
	Approved: true,
	LogoUrl:  "http://mock1/logo.svg",
}
var mockOrganization2 model.Organization = model.Organization{
	Oin:          "00000000000000000002",
	Name:         "mock-org 2",
	DiscoveryUrl: "http://mock2/v1",
	PublicKey: `-----BEGIN PUBLIC KEY-----
	MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxlibRfmDYIrD9MJRh0BS
	utHKiYlXeGopRHT2G8FK8WGvN2faR1ZFdWjTxgncmb1nds7OoG5u2E1KrDMiZ3SH
	M7T95Ynle1LHE9D0VhUXA0iAYOq4aumHiyHpmT8XQhRsF/3RjGoGagtnaeQ3uSyd
	NBkOiuVm/JTmcU3QDhLwj8st/GvxwaViuLsIH4WNX/LmkPkVbW5/CLNtD9mTmMgt
	x3tEBYHyIctPoBrXfYBaJN4tv6JY1vI37cafG4MBzunAr4iP/1GBhh6ql8bpdqd7
	cD6cHvGmlubrmdILm9V0erNDV9O9NrjxIalYRU/5E10qGIAuBdBubfOvJQX8QgFa
	WQIDAQAB
	-----END PUBLIC KEY-----`,
	Approved: false,
	LogoUrl:  "http://mock2/logo.svg",
}

// Helper function to create a new OrganizationDBClient and sqlmock
func _newOrganizationDBClientWithMock(t *testing.T) (*repositories.OrganizationDBClient, *sql.DB, sqlmock.Sqlmock, error) {
	logger := zaptest.NewLogger(t)

	mockDB, mock, _ := sqlmock.New()
	schemeDB := sqlx.NewDb(mockDB, "sqlmock")

	mock.ExpectPrepare(regexp.QuoteMeta(repositories.SQL_LIST_ORGANIZATIONS))
	mock.ExpectPrepare(regexp.QuoteMeta(repositories.SQL_CREATE_ORGANIZATION))
	mock.ExpectPrepare(regexp.QuoteMeta(repositories.SQL_READ_ORGANIZATION))
	mock.ExpectPrepare(regexp.QuoteMeta(repositories.SQL_UPDATE_ORGANIZATION))
	mock.ExpectPrepare(regexp.QuoteMeta(repositories.SQL_DELETE_ORGANIZATION))
	mock.ExpectPrepare(regexp.QuoteMeta(repositories.SQL_HEALTHCHECK_ORGANIZATION))

	organizationDBClient, err := repositories.NewOrganizationDBClient(context.Background(), logger, schemeDB)
	if err != nil {
		return nil, nil, nil, err
	}
	return organizationDBClient, mockDB, mock, nil
}

func Test_OrganizationDBClient_New(t *testing.T) {
	organizationDBClient, mockDB, _, err := _newOrganizationDBClientWithMock(t)
	if err != nil {
		t.Errorf("%s", err.Error())
	}
	defer mockDB.Close()
	assert.NotNil(t, organizationDBClient)
}

func Test_OrganizationDBClient_List(t *testing.T) {
	organizationDBClient, mockDB, mock, err := _newOrganizationDBClientWithMock(t)
	if err != nil {
		t.Errorf("%s", err.Error())
	}
	defer mockDB.Close()

	rows := sqlmock.NewRows([]string{"oin", "name", "discovery_url", "public_key", "approved", "logo_url"}).
		AddRow(
			mockOrganization1.Oin,
			mockOrganization1.Name,
			mockOrganization1.DiscoveryUrl,
			mockOrganization1.PublicKey,
			mockOrganization1.Approved,
			mockOrganization1.LogoUrl,
		).AddRow(
		mockOrganization2.Oin,
		mockOrganization2.Name,
		mockOrganization2.DiscoveryUrl,
		mockOrganization2.PublicKey,
		mockOrganization2.Approved,
		mockOrganization2.LogoUrl,
	)
	mock.ExpectQuery(regexp.QuoteMeta(repositories.SQL_LIST_ORGANIZATIONS)).WillReturnRows(rows)

	organizationsList, err := organizationDBClient.List(context.Background())
	if err != nil {
		t.Errorf("%s", err.Error())
	}
	if assert.NotNil(t, organizationsList) {
		assert.Len(t, *organizationsList, 2)

		firstOrganization := (*organizationsList)[0]
		assert.Equal(t, mockOrganization1.Oin, firstOrganization.Oin)
		assert.Equal(t, mockOrganization1.Name, firstOrganization.Name)
		assert.Equal(t, mockOrganization1.DiscoveryUrl, firstOrganization.DiscoveryUrl)
		assert.Equal(t, mockOrganization1.PublicKey, firstOrganization.PublicKey)
		assert.Equal(t, mockOrganization1.Approved, firstOrganization.Approved)
		assert.Equal(t, mockOrganization1.LogoUrl, firstOrganization.LogoUrl)

		secondOrganization := (*organizationsList)[1]
		assert.Equal(t, mockOrganization2.Oin, secondOrganization.Oin)
		assert.Equal(t, mockOrganization2.Name, secondOrganization.Name)
		assert.Equal(t, mockOrganization2.DiscoveryUrl, secondOrganization.DiscoveryUrl)
		assert.Equal(t, mockOrganization2.PublicKey, secondOrganization.PublicKey)
		assert.Equal(t, mockOrganization2.Approved, secondOrganization.Approved)
		assert.Equal(t, mockOrganization2.LogoUrl, secondOrganization.LogoUrl)
	}
}

func Test_OrganizationDBClient_Create(t *testing.T) {
	organizationDBClient, mockDB, mock, err := _newOrganizationDBClientWithMock(t)
	if err != nil {
		t.Errorf("%s", err.Error())
	}
	defer mockDB.Close()
	mock.ExpectExec(regexp.QuoteMeta(repositories.SQL_CREATE_ORGANIZATION)).WillReturnResult(sqlmock.NewResult(0, 1))

	createdOrganization, err := organizationDBClient.Create(context.Background(), mockOrganization1)
	if err != nil {
		t.Errorf("%s", err.Error())
	}
	if assert.NotNil(t, createdOrganization) {
		assert.Equal(t, mockOrganization1.Oin, createdOrganization.Oin)
		assert.Equal(t, mockOrganization1.Name, createdOrganization.Name)
		assert.Equal(t, mockOrganization1.DiscoveryUrl, createdOrganization.DiscoveryUrl)
		assert.Equal(t, mockOrganization1.PublicKey, createdOrganization.PublicKey)
		assert.Equal(t, mockOrganization1.Approved, createdOrganization.Approved)
		assert.Equal(t, mockOrganization1.LogoUrl, createdOrganization.LogoUrl)
	}
}

func Test_OrganizationDBClient_Get(t *testing.T) {
	organizationDBClient, mockDB, mock, err := _newOrganizationDBClientWithMock(t)
	if err != nil {
		t.Errorf("%s", err.Error())
	}
	defer mockDB.Close()

	rows := sqlmock.NewRows([]string{"oin", "name", "discovery_url", "public_key", "approved", "logo_url"}).
		AddRow(
			mockOrganization1.Oin,
			mockOrganization1.Name,
			mockOrganization1.DiscoveryUrl,
			mockOrganization1.PublicKey,
			mockOrganization1.Approved,
			mockOrganization1.LogoUrl,
		)
	mock.ExpectQuery(regexp.QuoteMeta(repositories.SQL_READ_ORGANIZATION)).WillReturnRows(rows)

	organization, err := organizationDBClient.Get(context.Background(), mockOrganization1.Oin)
	if err != nil {
		t.Errorf("%s", err.Error())
	}
	if assert.NotNil(t, organization) {
		assert.Equal(t, mockOrganization1.Oin, organization.Oin)
		assert.Equal(t, mockOrganization1.Name, organization.Name)
		assert.Equal(t, mockOrganization1.DiscoveryUrl, organization.DiscoveryUrl)
		assert.Equal(t, mockOrganization1.PublicKey, organization.PublicKey)
		assert.Equal(t, mockOrganization1.Approved, organization.Approved)
		assert.Equal(t, mockOrganization1.LogoUrl, organization.LogoUrl)
	}
}

func Test_OrganizationDBClient_Update(t *testing.T) {
	organizationDBClient, mockDB, mock, err := _newOrganizationDBClientWithMock(t)
	if err != nil {
		t.Errorf("%s", err.Error())
	}
	defer mockDB.Close()
	mock.ExpectExec(regexp.QuoteMeta(repositories.SQL_UPDATE_ORGANIZATION)).WillReturnResult(sqlmock.NewResult(0, 1))

	err = organizationDBClient.Update(context.Background(), mockOrganization1, mockOrganization1.Oin)
	if err != nil {
		t.Errorf("%s", err.Error())
	}
}

func Test_OrganizationDBClient_Delete(t *testing.T) {
	organizationDBClient, mockDB, mock, err := _newOrganizationDBClientWithMock(t)
	if err != nil {
		t.Errorf("%s", err.Error())
	}
	defer mockDB.Close()
	mock.ExpectExec(regexp.QuoteMeta(repositories.SQL_DELETE_ORGANIZATION)).WillReturnResult(sqlmock.NewResult(0, 1))

	err = organizationDBClient.Delete(context.Background(), mockOrganization1.Oin)
	if err != nil {
		t.Errorf("%s", err.Error())
	}
}

func Test_OrganizationDBClient_HealthCheck(t *testing.T) {
	organizationDBClient, mockDB, mock, err := _newOrganizationDBClientWithMock(t)
	if err != nil {
		t.Errorf("%s", err.Error())
	}
	defer mockDB.Close()
	mock.ExpectExec(regexp.QuoteMeta(repositories.SQL_HEALTHCHECK_ORGANIZATION)).WillReturnResult(sqlmock.NewResult(0, 0))

	organizationDBClient.GetHealthCheck()
	assert.NotNil(t, organizationDBClient)
}
