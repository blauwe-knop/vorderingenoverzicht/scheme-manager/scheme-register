package model

type Organization struct {
	Oin          string `json:"oin"`
	Name         string `json:"name"`
	DiscoveryUrl string `json:"discoveryUrl" db:"discovery_url"`
	PublicKey    string `json:"publicKey" db:"public_key"`
	Approved     bool   `json:"approved"`
	LogoUrl      string `json:"logoUrl" db:"logo_url"`
}
